﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace allegretto.API.VK.Models
{

    public class BaseResponse
    {
        public object response { get; set; }
        public ErrorResponse error { get; set; }
    }

    public class ErrorResponse
    {
        public int error_code { get; set; }
        public string error_msg { get; set; }
        public string captcha_sid { get; set; }
        public string captcha_img { get; set; }
    }

    public class MessagesHelper : BaseResponse
    {
        public Messages response { get; set; }
    }

    public class AudioLyricsResponse : BaseResponse
    {
        public AudioLyricsClass response { get; set; }
    }

    public class AudioResponse : BaseResponse
    {
        public Audios response { get; set; }
    }

    public class LongPollServerResponse : BaseResponse
    {
        public LongPollServer response { get; set; }
    }

    public class LongPollServer
    {
        public string key { get; set; }
        public string server { get; set; }
        public int ts { get; set; }

        public void SetTS(int tsq) { ts = tsq; }
    }

    public class LongPollAnswer
    {
        public int ts { get; set; }

        public object[][] updates { get; set; }
        public int failed { get; set; }
    }

    public class Audios
    {
        public int count { get; set; }
        public BaseAudio[] items { get; set; }
    }

    public class AudioLyricsClass
    {
        public int lyrics_id { get; set; }
        public string text { get; set; } // get text
        public string GetText() { if (text.Length == 0)return "не найдено"; return text; } 
    }

    public class ExecuteResponse : BaseResponse
    {
        public Execute response { get; set; }
    }

    public class Execute
    {
        public Friends friends { get; set; }
        public User user { get; set; }
    }
    public class BasePhoto
    {
        public int id { get; set; }
        public int album_id { get; set; }
        public int owner_id { get; set; }
        public string photo_75 { get; set; }
        public string photo_130 { get; set; }
        public string photo_604 { get; set; }
        public string photo_807 { get; set; }
        public string photo_1280 { get; set; }
        public string photo_2560 { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public string text { get; set; }
        public int date { get; set; }
        public string access_key { get; set; }
    }

    public class BaseVideo
    {
        public int id { get; set; }
        public int owner_id { get; set; }
        public string title { get; set; }
        public int duration { get; set; }
        public string description { get; set; }
        public int date { get; set; }
        public int views { get; set; }
        public int comments { get; set; }
        public string photo_130 { get; set; }
        public string photo_320 { get; set; }
        public string access_key { get; set; }
    }
    public class BaseAttachment
    {
        public string type { get; set; }
        public BasePhoto photo { get; set; }
        public BaseVideo video { get; set; }
        public BaseAudio audio { get; set; }
    }

    public class UserStatus : BaseResponse
    {
        public Status response { get; set; }
    }

    public class Users : BaseResponse
    {
        public User[] response { get; set; }
    }

    public class Friends
    {
        public int count { get; set; }
        public User[] items { get; set; }

        public int GetOnlineCount()
        {
            int online = 0;
            for (int i = 0; i < items.Length; i++)
                if (items[i].online == 1 || items[i].online_mobile == 1)
                    online++;
            return online;
        }
    }

    public class Messages
    {
        public int count { get; set; }
        public Message[] items { get; set; }
    }

    public class ForwardMessage
    {
        public int user_id { get; set; }
        public int date { get; set; }
        public string body { get; set; }
        public ForwardMessage[] fwd_messages { get; set; }
    }

    public class Message
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public int date { get; set; }
        public int read_state { get; set; }
        public int @out { get; set; }
        public string title { get; set; }
        public string body { get; set; }
        public BaseAttachment[] attachments { get; set; }
        public Message[] fwd_messages { get; set; }
        public int emoji { get; set; }
        public int important { get; set; }
        public int deleted { get; set; }

        public string GetDate(bool isChat = false)
        {
            DateTime test = Misc.UnixTimeToDateTime(date.ToString()).AddHours(MainWindow.utcoffset);
            if (!isChat)
            {


                int days = (int)(DateTime.Now - test).TotalDays;


                if (test.Day == DateTime.Now.Day) return "сегодня в " + test.ToShortTimeString(); // CO: today
                if (days < 1) { return "вчера в " + test.ToShortTimeString(); } // CO: yesterday
                return test.ToShortDateString();
            }
            else { return test.ToShortTimeString(); }
        }

        public static Message FromForward(Message fwd) //fwdmessage
        {
            Message msg = new Message();
            msg.user_id = fwd.user_id;
            msg.date = fwd.date;
            msg.body = fwd.body;
            msg.fwd_messages = fwd.fwd_messages;
            return msg;
        }
    }
    public class LastSeen
    {
        public int time { get; set; }
        public int platform { get; set; }
    }
    public class User
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public int sex { get; set; }
        public string bdate { get; set; }
        public string city { get; set; }
        public int county { get; set; }
        public string photo_50 { get; set; }
        public string photo_100 { get; set; }
        public string photo_200 { get; set; }
        public string photo_max { get; set; }
        public string photo_200_orig { get; set; }
        public string photo_400_orig { get; set; }
        public int online { get; set; }
        public int online_mobile { get; set; }
        public string domain { get; set; }
        public int has_mobile { get; set; }
        public string site { get; set; }
        public string status { get; set; }
        public string deactivated { get; set; }
        public LastSeen last_seen { get; set; }
        public int[] lists { get; set; }
        public Counters counters { get; set; }

        public int GetStatus()
        {
            if (online_mobile != 0) return 2;
            if (online != 0) return 1;

            return 0;
        }
        public string GetName() { return first_name + " " + last_name; }
        public string GetLastSeenTime()
        {
            if (deactivated != null) // .net sux! but i still love dat shit <3
            {
                return "a long time ago in a galaxy far far away...";
            }
            else
            {
                DateTime test = Misc.UnixTimeToDateTime(last_seen.time.ToString()).AddHours(MainWindow.utcoffset);
                int days = (int)(DateTime.Now - test).TotalDays;


                if (test.Day == DateTime.Now.Day) return "сегодня в " + test.ToShortTimeString(); // today
                if (days < 1) { return "вчера в " + test.ToShortTimeString(); } // yesterday
                return test.ToShortDateString();
            }
        }
    }

    public class Counters
    {
        public int friends { get; set; } //norm
        public int online_friends { get; set; }
    }


    public class Status
    {
        public string text { get; set; }
        public BaseAudio audio { get; set; }
    }

    public class BaseAudio
    {
        public int id { get; set; }
        public int owner_id { get; set; }
        public string artist { get; set; }
        public string title { get; set; }
        public int duration { get; set; }
        public string url { get; set; }
        public int genre_id { get; set; }
        public int lyrics_id { get; set; }
        public int album_id { get; set; }
        public string text { get; set; }

        public string GetName() { return artist + " - " + title; }
        public string GetNameTrim() {
            if (GetName().Length > 44)
                return GetName().Substring(0, 44) + "...";
            return GetName();
        }
    }
}

namespace allegretto.API.VK
{
    using Newtonsoft.Json;
    using System.Collections.Specialized;
    using System.Diagnostics;
    using System.IO;
    using System.Net;
    using System.Reflection;
    using System.Threading;
    using System.Windows;
    using System.Windows.Forms;
    public class Logic
    {

        public string args = "";
        public int argsCount = 0;

        public T request<T>(string method, string args = "")
        {
            // if (MainWindow.isLocked)
            //     Thread.CurrentThread.Suspend(); //meh

            T obj = json<T>(method, args);
            Models.BaseResponse test = superiorHack(obj);
            if (test.error != null)
            {
                if (test.error.captcha_sid != null)
                {

                }
            }
            return obj;
        }

        public Models.BaseResponse superiorHack(object obj)
        {
            return (Models.BaseResponse)obj;
        }

        public T json<T>(string method, string args = "")
        {
            if (args.Length == 0)
            {
                this.appendArg("v", "5.5");
                this.appendArg("lang", "ru");
                this.appendArg("access_token", MainWindow.token);
            }
            string answer = "";

            string url = "https://api.vk.com/method/" + method + "?" + (this.args.Length > 0 ? this.args : "") + args;
            Console.WriteLine(DateTime.Now.ToString() + " - " + url);
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Proxy = null;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Encoding enc = System.Text.Encoding.UTF8;
                StreamReader loResponseStream = new StreamReader(response.GetResponseStream(), enc);
                answer = System.Web.HttpUtility.HtmlDecode(loResponseStream.ReadToEnd()); // JSON
                Debug.WriteLine(answer);
                if (answer.Contains("\n")) answer = answer.Replace("\n", "\r\n");
                resetArgs();
            }
            catch (Exception ex) { MainWindow.isLocked = true; if (ex.GetType() == typeof(System.Net.WebException)) { System.Windows.Forms.MessageBox.Show("Невозможно достичь ВКонтакте.\r\nПроверьте Ваш статус соединения и повторите попытку.", "VKDesktop - Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning); } Debug.WriteLine("\r\n" + ex); }          //  Debug.WriteLine(answer);

            T obj = JsonConvert.DeserializeObject<T>(answer);

            return obj;
        }

        public Logic appendArg(string name, string value)
        {
            this.args += (this.argsCount > 0 ? "&" : "") + name + "=" + value;
            this.argsCount++;
            return this;
        }

        public Logic resetArgs()
        {
            this.args = "";
            this.argsCount = 0;
            this.appendArg("v", "5.5");
            this.appendArg("lang", "ru");
            this.appendArg("access_token", MainWindow.token);
            return this;
        }

        public static Logic getInstance()
        {
            return new Logic();
        }
    }


    public class BaseLogic
    {
        public string args;
        public Logic vk;

        public BaseLogic()
        {
            this.vk = Logic.getInstance();
        }

        public void appendArg(string name, string value)
        {
            vk.appendArg(name, value);
        }

        public void afterCall()
        {
        }
        public void beforeCall() { }
    }

    public class Messages : BaseLogic
    {
        //m = new Messages
        //m.appendArg("out", "1")KO
        //m.get();
        public Models.MessagesHelper getHistory(int user_id = 0, int count = 20)
        {
            vk.appendArg("user_id", user_id.ToString());
            vk.appendArg("count", count.ToString());

            return vk.request<Models.MessagesHelper>("messages.getHistory");
        }

    }

    public class Execute : BaseLogic
    {
        public Models.ExecuteResponse init(int count = 15, int offset = 0)
        {
            vk.appendArg("count", count.ToString());
            vk.appendArg("offset", offset.ToString());

            return vk.request<Models.ExecuteResponse>("execute.init");
        }
    }

    public class Audio : BaseLogic
    {
        public Models.AudioResponse get(int count = 50, int offset = 0)
        {
            

            vk.appendArg("count", count.ToString());
            vk.appendArg("offset", offset.ToString());

            return vk.request<Models.AudioResponse>("audio.get");
        }

        public Models.AudioResponse search(string query = "")
        {
            vk.appendArg("q", query.ToString());
            return vk.request<Models.AudioResponse>("audio.search");
        }

    }

    public class AudioLyrics : BaseLogic
    {
        public Models.AudioLyricsResponse getLyrics(int lyrics_id = 1)
        {
            vk.appendArg("lyrics_id", lyrics_id.ToString());
            return vk.request<Models.AudioLyricsResponse>("audio.getLyrics");
        }
    }

    public class UserGet : BaseLogic
    {
        public Models.Users get(int user_id = 0)
        {
            vk.appendArg("user_ids", user_id.ToString());
            vk.appendArg("fields", "status,online,last_seen");
            return vk.request<Models.Users>("users.get");
        }
    }

    public class LongPoll : BaseLogic
    {
        public Models.LongPollServerResponse getLongPollServer()
        {
            return vk.request<Models.LongPollServerResponse>("messages.getLongPollServer");
        }

        public Models.LongPollAnswer getLongPoll(Models.LongPollServer serverData)
        {
            var request = (HttpWebRequest)WebRequest.Create("http://" + serverData.server + "?act=a_check&key=" + serverData.key + "&ts=" + serverData.ts + "&wait=25&mode=64");
            request.KeepAlive = true;
            using (var response = (HttpWebResponse)request.GetResponse())
            {

                StreamReader loResponseStream = new StreamReader(response.GetResponseStream(), System.Text.Encoding.UTF8);
                var q = System.Web.HttpUtility.HtmlDecode(loResponseStream.ReadToEnd());
                return JsonConvert.DeserializeObject<Models.LongPollAnswer>(q);
            }
        }
    }
}