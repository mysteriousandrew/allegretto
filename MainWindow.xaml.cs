﻿using allegretto.API;
using allegretto.API.VK;
using allegretto.API.VK.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Runtime.InteropServices;

namespace allegretto
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /* Thanks to MSDN, CODEPROJECT and some other nice sites.
         * I did this, because I really love music. And programming, of course.
         * OOP in API.cs belongs to @getjump (he helped me a lot)
         * I have no idea how to XAML, so most of UI elements are copypasted. lol.
         * vk.com/leettner || www.littner.me
         * */

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);
        private static IntPtr _hookID = IntPtr.Zero;

        static bool isPaused;

        private static allegretto.API.KeyboardH.LowLevelKeyboardProc _proc = HookCallback;

        private static IntPtr HookCallback(int nCode, IntPtr wParam, IntPtr lParam)
        {
            if (nCode >= 0 && wParam == (IntPtr)0x0100)
            {
                int vkCode = Marshal.ReadInt32(lParam);
               // Debug.WriteLine((System.Windows.Forms.Keys)vkCode + " - " + vkCode);
                switch (vkCode)
                {
                    case 176:
                        frm.Play(frm.current_playing + 1, true);
                        break;
                    case 177:
                        frm.Play(frm.current_playing - 1, true);
                       break;
                    case 178:
                       frm.mediaElement1.Stop();
                        break;
                    case 179:
                      
                        if (isPlaying)
                            frm.mediaElement1.Pause();
                        else
                            frm.mediaElement1.Play();
                        isPlaying = !isPlaying;
                        break;

                }
                //if(vkCode == 176)
                    //Play(current_playing + 1, true);
                
                
            }
            return CallNextHookEx(_hookID, nCode, wParam, lParam);
        }


      

        public static string token { get; set; }
        public static int utcoffset { get; set; }
        public static bool authsuccess { get; set; }
        public static bool isLocked { get; set; }
        public static MainWindow frm { get; set; }

        BackgroundWorker bwLoader = new BackgroundWorker();

        static Auth AuthF;
        public MainWindow()
        {

            this.Hide();
            InitializeComponent();

            frm = this;
            AuthF = new Auth();
            AuthF.ShowDialog(); // open Auth form, that contains a browser to proceed auth and some other things (such as token cache)
        }
        public static void OpenMainWindow()
        {
            KeyboardH kh = new KeyboardH();
            kh.DoSwag(_proc);


            AuthF.Close();
            frm.Show();
            frm.Logic();
        }

        List<BaseAudio> AudioData, m_aBackup;
        AudioResponse m_aResponse ;

        void Logic()
        {
            frm.WindowState = WindowState.Normal;

            Audio audio = new Audio();
            AudioData = new List<BaseAudio>();
            m_aResponse = audio.get(); // load first 50 songs
            AudioData.AddRange(m_aResponse.response.items);
            for (int i = 0; i < AudioData.Count; i++)
                listBox1.Items.Add(AudioData[i].GetNameTrim());

            new Thread(BackgroundLoad).Start();
            new Thread(OnProgressChanged).Start();
        }

        void BackgroundLoad()
        {
            Audio audio = new Audio(); // updated 'AudioData'
            int max = m_aResponse.response.count;
            int offset = 50;
           // int curr = 0;

            while (AudioData.Count < max) // !=
            {
                m_aResponse = audio.get(150, offset);
                AudioData.AddRange(m_aResponse.response.items);
                for (int i = 0; i < m_aResponse.response.items.Length; i++) UiInvoke(() =>
                {
                    listBox1.Items.Add(m_aResponse.response.items[i].GetNameTrim());
                    //listBox1.Items.Refresh();
                });
                offset += 150;

              
                Thread.Sleep(500);
            }
          

            m_aBackup = AudioData;

            Thread.CurrentThread.Abort();

        }


        void lbi_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

            if (listBox1.SelectedIndex < 0)
                Play();
            else
                Play(listBox1.SelectedIndex, true);
        }


        static bool isPlaying = false;
        int current_playing = -1;

        private void BtnPlay_Click(object sender, RoutedEventArgs e)
        {

            if (listBox1.SelectedIndex < 0)
                Play();
            else
                Play(listBox1.SelectedIndex);
        }

        private void volume_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            mediaElement1.Volume = volume.Value;
        }

        void doStuff(object tosearch)
        {
            // BitmapImage test = ;
            UiInvoke(() => AlbumArt.Source = new BitmapImage(new Uri(Misc.GetArt(tosearch.ToString()), UriKind.Absolute)));
            Thread.CurrentThread.Abort();
        }


        string SongDurationString = "9:99:99";
        private void Play(int index = 0, bool newTrack = false)
        {
            BaseAudio song = AudioData[index];


            if (isReplay)
            {
                song = m_aBackup[index]; // i did this, because i had no idea how else could I keep the song that was loaded from web (vk audio search)
            }

            listBox1.SelectedIndex = index;



           

            if (!isPlaying || newTrack) // i forgot why
            {
                TestBtn.Source = new BitmapImage(new Uri("pack://siteoforigin:,,,/Resources/pause.png"));
                if (!mediaElement1.CanPause || newTrack) // just checkin' if we already have the song
                    mediaElement1.Source = new Uri(AudioData[index].url);

                mediaElement1.Play();


                if (song.lyrics_id != 0) // get song's lyrics
                {
                    AudioLyrics al = new AudioLyrics();
                    AudioLyricsResponse alr = al.getLyrics(song.lyrics_id);
                    LyricsHolder.Text = alr.response.GetText();
                }
                else { LyricsHolder.Text = " "; }

                if (isStreaming) // should we stream our audio to vk || not
                {
                    Misc.SendReq("audio.setBroadcast?audio=" + song.owner_id + "_" + song.id);
                }




                isPlaying = true;



                SongDurationString = Misc.ParseDuration(song.duration).ToString();

                TotalTime.Text = SongDurationString + "/" + SongDurationString;
                SongArtist.Text = song.artist;
                SongTitle.Text = song.title;
                progress.Maximum = song.duration;

                if (current_playing != index)
                {
                    Thread ds = new Thread(doStuff); // load album art
                    ds.Start(song.GetName());
                }
                current_playing = index; // kiss me, later

            }


        }



        public string SongPositionStr() // i was in India once
        {
            TimeSpan SongPos = mediaElement1.Position;
            string time = "";
            string sec = SongPos.Seconds.ToString();
            string m = SongPos.Minutes.ToString();
            string h = SongPos.Hours.ToString();
            if (Convert.ToInt16(sec) < 10) sec = "0" + sec;
            if (Convert.ToInt16(m) < 10) m = "0" + m;
            if (Convert.ToInt16(h) < 1) time = m + ":" + sec;
            if (Convert.ToInt16(h) > 1) time = h + ":" + m + ":" + sec;
            return time;
        }


        private void OnProgressChanged()
        {

            while (true) // never seen that kind of shit btw
            {
                if (!iSbusy) // checking if 'song played time' bar is being used, if it's not then continue to display progress
                {
                    // NOTE: move this to bottom
                    UiInvoke(() => TotalTime.Text = SongPositionStr() + "/" + SongDurationString);
                    UiInvoke(() => progress.Value = mediaElement1.Position.TotalSeconds); // PBAR

                    UiInvoke(new Action(() =>
                    {
                        if (mediaElement1.HasAudio && Convert.ToInt32(mediaElement1.Position.TotalSeconds) >= progress.Maximum)
                        {
                            if (!isReplay)
                                Play(current_playing + 1, true);
                            else
                                Play(current_playing, true);
                        }

                    }));
                }
                else { UiInvoke(() => mediaElement1.Position = TimeSpan.FromSeconds(progress.Value)); }
                Thread.Sleep(1); // NEVER ERASE THIS LINE

            }

        }

        private void BtnNext_Click(object sender, RoutedEventArgs e)
        {
            Play(current_playing + 1, true);
        }

        private void BtnPrev_Click(object sender, RoutedEventArgs e)
        {
            if (current_playing <= 0) // haha
                return;
            Play(current_playing - 1, true);
        }

        public static void UiInvoke(Action a)
        {
            Application.Current.Dispatcher.Invoke(a);
        }



        private DateTime downTime;
        private object downSender;
        private void TestBtn_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                this.downSender = sender;
                this.downTime = DateTime.Now;
            }
        }

        private void TestBtn_MouseUp_1(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Released && sender == this.downSender)
            {
                TimeSpan timeSinceDown = DateTime.Now - this.downTime;
                if (timeSinceDown.TotalMilliseconds < 500)
                {
                    if (!isPlaying)
                    {
                        TestBtn.Source = new BitmapImage(new Uri("pack://siteoforigin:,,,/Resources/pause.png"));
                        if (listBox1.SelectedIndex < 0)
                            Play();
                        else
                            Play(listBox1.SelectedIndex);
                    }
                    else
                    {
                        TestBtn.Source = new BitmapImage(new Uri("pack://siteoforigin:,,,/Resources/play.png"));
                        mediaElement1.Pause();
                        isPlaying = false;
                    }
                }
            }
        }

        private void listBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }


        public bool Dragging;
        Point PointClicked;

        int pos;

        bool iSbusy = false;



        private void progress_PreviewMouseDown_1(object sender, MouseButtonEventArgs e)
        {
            iSbusy = true;
        }

        private void progress_PreviewMouseUp_1(object sender, MouseButtonEventArgs e)
        {
            iSbusy = false;
        }


        bool isStreaming = false;
        private void BtnStream_MouseUp_1(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Released && sender == this.downSender)
            {
                TimeSpan timeSinceDown = DateTime.Now - this.downTime;
                if (timeSinceDown.TotalMilliseconds < 500)
                {
                    if (!isStreaming)
                    {
                        BtnStream.Source = new BitmapImage(new Uri("pack://siteoforigin:,,,/Resources/stream_enabled.png")); // enabled
                        isStreaming = true;
                        Misc.SendReq("audio.setBroadcast?audio=" + AudioData[current_playing].owner_id + "_" + AudioData[current_playing].id);
                    }
                    else
                    {
                        BtnStream.Source = new BitmapImage(new Uri("pack://siteoforigin:,,,/Resources/stream_disabled.png")); // disabled
                        isStreaming = false;
                        Misc.SendReq("audio.setBroadcast?audio=");
                    }
                }
            }
        }


        bool isReplay;
        private void BtnReplay_MouseUp_1(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Released && sender == this.downSender)
            {
                TimeSpan timeSinceDown = DateTime.Now - this.downTime;
                if (timeSinceDown.TotalMilliseconds < 500)
                {
                    if (!isReplay)
                    {
                        BtnReplay.Source = new BitmapImage(new Uri("pack://siteoforigin:,,,/Resources/repeat_enabled.png")); // enabled
                        isReplay = true;
                    }
                    else
                    {
                        BtnReplay.Source = new BitmapImage(new Uri("pack://siteoforigin:,,,/Resources/repeat_disabled.png")); // disabled
                        isReplay = false;
                    }
                }
            }
        }

        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Search();
            if (SearchBox.Text.Length == 0) // so i thought i could reset our updated AudioData
            {
                var b = AudioData;
                AudioData = m_aBackup;
                m_aBackup = b;
                if (AudioData.Count != listBox1.Items.Count)
                {
                    listBox1.Items.Clear();
                    for (int i = 0; i < AudioData.Count; i++)
                        listBox1.Items.Add(AudioData[i].GetName());

                    listBox1.SelectedIndex = 0;
                    listBox1.ScrollIntoView(listBox1.Items.GetItemAt(0));
                }
            }
        }


        bool Search(int start = 0, bool local = true)
        {
            if (local) // search in user's playlist
            {
                if (SearchBox.Text.Length > 0)
                {
                    for (int i = start; i < listBox1.Items.Count - 1; i++)
                    {
                        if (listBox1.Items[i].ToString().ToLower().Contains(SearchBox.Text.ToLower()))
                        {
                            if (i == listBox1.SelectedIndex)
                            {
                                listBox1.ScrollIntoView(listBox1.Items.GetItemAt(i));
                                continue;
                            }
                            listBox1.SelectedIndex = i;
                            listBox1.ScrollIntoView(listBox1.Items.GetItemAt(i));
                            return true;
                        }
                    }
                }
            }
            else // not local search
            {

             /*   AudioResponse upd = new Audio().search(SearchBox.Text);
                AudioData = upd;
                listBox1.Items.Clear();
                for (int i = 0; i < upd.response.items.Length; i++)
                    listBox1.Items.Add(upd.response.items[i].GetName());*/
            }
            return false;
        }

        private void SearchBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) && Keyboard.IsKeyDown(Key.Enter))
            {
                Search(0, false);
            }
            else if (e.Key == Key.Enter)
                if (!Search(listBox1.SelectedIndex)) Search(0);

        }

        private void Window_Closing_1(object sender, CancelEventArgs e)
        {
            mediaElement1.Stop();
            Misc.SendReq("audio.setBroadcast?audio="); // let it revert
        }

        private void TestBtn_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {

        }



    }

}
