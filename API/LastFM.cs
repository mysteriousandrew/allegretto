﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;



namespace allegretto.API
{
    class LastFM
    {
        public class LastFMRoot
        {
            public string token { get; set; }
        }

        public LastFMRoot sendReq(string reqmtd, string fields, bool dbg = false) // return json of/with => token, requested API method
        {

            try
            {
                string testuri = "http://ws.audioscrobbler.com/2.0/?method=auth.gettoken&api_key=1537810e299d81cd3cccad7a3552f763&format=json";
            
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(testuri);
                request.Proxy = null;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Encoding enc = System.Text.Encoding.UTF8;
                StreamReader loResponseStream = new StreamReader(response.GetResponseStream(), enc);
                return JsonConvert.DeserializeObject<LastFMRoot>(loResponseStream.ReadToEnd());
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
            return null;
        }
    }
}
