﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using allegretto.API.VK;
using allegretto.API;
using System.Diagnostics;

namespace allegretto
{
    public partial class Auth : Form
    {


        public Auth()
        {

            InitializeComponent();
        }

        private void Auth_Load(object sender, EventArgs e)
        {
            TimeZone localZone = TimeZone.CurrentTimeZone;
            MainWindow.utcoffset = localZone.GetUtcOffset(DateTime.Now).Hours;
            authBrowser.ScriptErrorsSuppressed = true;
            AuthQ();
        }
        public void AuthQ(bool isforced = false)
        {
            MainWindow.authsuccess = true;
            AuthRequest VKAuth = new AuthRequest();
            if (!checkTokenCache() || !MainWindow.authsuccess || isforced)
            {
                this.Show();
                VKAuth.Auth(authBrowser, "friends,status,messages,offline,audio", 3736196); // browser (to proceed auth), rights, appid
            }
            else { this.WindowState = FormWindowState.Minimized; MainWindow.OpenMainWindow(); }
        }
        bool checkTokenCache()
        {
            string test = "";
            string expire = "";

            test = Encoding.ASCII.GetString(Convert.FromBase64String(Cache.loadfromSettings("TOKEN", "value").ToString()));
            expire = Cache.loadfromSettings("TOKEN", "end").ToString();

            if (test.Length > 10)
            {

                MainWindow.token = test;
                return true;
            }
            return false;
        }


        private void authBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            try
            {
                Debug.WriteLine(e.Url);
                var urlParams = System.Web.HttpUtility.ParseQueryString(e.Url.Fragment.Substring(1));
                string url = authBrowser.Url.ToString();
                if (url.Contains("access_token"))
                {

                    MainWindow.token = urlParams.Get("access_token");
                    Cache.saveToSettings("TOKEN", "value", Convert.ToBase64String(Encoding.ASCII.GetBytes(MainWindow.token)));
                    if (urlParams.Get("expires_in") == "0") Cache.saveToSettings("TOKEN", "end", "never");
                   // this.Hide();
                    this.WindowState = FormWindowState.Minimized;
                    MainWindow.OpenMainWindow();
                }
            }
            catch (Exception) { authBrowser.Refresh(); }
        }
    }
}
