﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace allegretto.API
{
    class inicontrol
    {
        private String File = "";

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileInt(String Section, String Key, int Value, String FilePath);
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(String Section, String Key, String Value, String FilePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileInt(String Section, String Key, int Default, String FilePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(String Section, String Key, String Default, StringBuilder retVal, int Size, String FilePath);

        public inicontrol(String IniFile)
        {
            this.File = IniFile;
        }

        public String ReadString(String Section, String Key, String Default)
        {
            StringBuilder StrBu = new StringBuilder(255);
            GetPrivateProfileString(Section, Key, Default, StrBu, 255, File);
            return StrBu.ToString();
        }

        public int ReadInt(String Section, String Key, int Default)
        {
            return GetPrivateProfileInt(Section, Key, Default, File);
        }

        public void WriteString(String Section, String Key, String Value)
        {
            WritePrivateProfileString(Section, Key, Value, File);
        }

        public void WriteInt(String Section, String Key, int Value)
        {
            WritePrivateProfileInt(Section, Key, Value, File);
        }

        //  public string pk3y = "m8WAfxVA!4Gvavj$";
    }
    class Cache
    {
        static string pathset = System.Windows.Forms.Application.StartupPath + @"\settings.cfg";
        public static object loadfromSettings(string cat, string subcat)
        {
            if (!System.IO.File.Exists(pathset)) { var ww = System.IO.File.Create(pathset); ww.Close(); }
            var cfg = new inicontrol(pathset);
            return cfg.ReadString(cat, subcat, null);
        }

        public static void saveToSettings(string cat, string subcat, string q)
        {

            if (!System.IO.File.Exists(pathset))
            {
                var ww = System.IO.File.Create(pathset);
                ww.Close();
            }
            var cfg = new inicontrol(pathset);
            cfg.WriteString(cat, subcat, q);

        }

        public static void writeDriver(string name = null, string status = null)
        {
            saveToSettings("PROFILE", "name", name);
            saveToSettings("PROFILE", "status", status);
        }

        public static void loadDriver(ref Label name, ref Label stat, ref Image avatar)
        {
            name.Text = loadfromSettings("PROFILE", "name").ToString();
            stat.Text = loadfromSettings("PROFILE", "status").ToString();

            // avatar.LoadAsync(Application.StartupPath + @"\cache\avatars\driver.jpg");
        }

    }
}
